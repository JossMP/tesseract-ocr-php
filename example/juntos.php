<?php
require_once(dirname(__DIR__) . "/vendor/autoload.php");


//$test = new \jossmp\ocr\Image(__DIR__ . "/img/captcha.jpeg");
$test = new \jossmp\ocr\SimpleOCR();

$success = 0;
$fail = 0;
$a = time();
$files = glob("juntos/0*.png");
foreach ($files as $file) {
    $captcha = $test
        ->set_image_path($file)
        ->set_max_grey(50)
        ->set_min_grey(0)
        ->rgb2hash()
        ->remove_noise()
        //->dump_hash()
        //->save_image(__DIR__ . "/temp.png")
        //->extract_letters()
        ->tesseract()
        ->whitelist(range('a', 'z'), range('A', 'Z'), '0123456789')
        ->run();

    if ($captcha && strlen(trim($captcha)) == 4) {
        rename($file, dirname($file) . "/" . $captcha . ".png");
        echo "\nSuccess (" . $success++ . ")=> " . trim($captcha);
    } else {
        echo "\nFail (" . $fail++ . ")=> " . trim($captcha . " ");
    }
    echo " => " . basename($file);
}
echo "\n";
$b = time();
$total = $success + $fail;
echo "\nTotal: " . $total;
echo "\nsuccess: " . $success;
echo "\nFails: " . $fail;
echo "\nTime: " . $b - $a . "seg";
