<?php
function descarga($name=0)
{
	$url = 'https://app.juntos.gob.pe/SIGO/Login/Captcha';
	$nombre_archivo = $name.'.png';

	$imagen = file_get_contents($url);

	if ($imagen !== false) {
	    // Guardar la imagen en un archivo en el servidor
	    file_put_contents($nombre_archivo, $imagen);
	    echo 'La imagen ha sido descargada correctamente.';
	} else {
	    echo 'Hubo un error al intentar descargar la imagen.';
	}
}

$cant=0;
while($cant<200){
	$name= str_pad($cant,4,"0",STR_PAD_LEFT);
	descarga($name);
	$cant++;
}
