<?php
require_once(dirname(__DIR__) . "/vendor/autoload.php");

$test = new \jossmp\ocr\SimpleOCR();
$dictionary = include(__DIR__ . '/dictionary_soat.php');
$test->set_image_string(file_get_contents(__DIR__ . "/soat/soat.png"), null)
    ->set_max_grey(87)
    ->set_min_grey(82)
    ->add_letter(4, 2, 24, 26)
    ->add_letter(31, 7, 24, 26)
    ->add_letter(55, 2, 24, 26)
    ->add_letter(80, 7, 24, 26)
    ->add_letter(105, 2, 24, 26)
    ->add_letter(127, 2, 24, 26);

$str = trim($test->solver($dictionary));

if (strlen($str) == 6) {
    echo $str;
}
