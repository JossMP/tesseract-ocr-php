<?php
require_once(dirname(__DIR__) . "/vendor/autoload.php");


//$test = new \jossmp\ocr\Image(__DIR__ . "/img/captcha.jpeg");
$test = new \jossmp\ocr\Image(__DIR__ . "/img/sunedu-4.png");
echo $test->set_max_grey(74)
	->set_min_grey(0)
	->set_min_width_letter(10)
	->set_min_height_letter(15)
	->rgb2hash()
	->extract_letters()
	->tesseract()
	->whitelist(range('a', 'z'), range('A', 'Z'), '0123456789')
	->run();

echo "\n-----|\n";
$temp = __DIR__ . "/img/sunedu-4.png";
$tesseractOCR = new \jossmp\ocr\TesseractOCR();
echo $tesseractOCR->image($temp)->run();

echo "\n";
