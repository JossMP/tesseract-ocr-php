<?php

namespace jossmp\ocr;

use Exception;
use jossmp\ocr\TesseractOCR;
use jossmp\ocr\FriendlyErrors;

class Image
{
    public $letter_width = 30;
    public $letter_height = 30;
    public $max_grey = 255;
    public $min_grey = 0;

    public $max_noise = 30;

    public $min_width_letter = 10;
    public $min_height_letter = 15;

    private $_image;
    private $_image_w;
    private $_image_h;

    private $_new_image;
    private $_new_image_w = 0;
    private $_new_image_h = 0;
    private $_hash_data = array();
    private $_hash_data_grey = array();
    private $_hash_letters = array();
    private $_hash_letter = array();
    private $_hash_tmp = array();

    private $_save_image;

    private $_count_letter = 0;

    private $_tesseractOCR = NULL;

    public $path_image = NULL;
    public $path_image_tmp = NULL;

    public function __construct($path_image)
    {
        FriendlyErrors::checkImagePath($path_image);

        $this->path_image = $path_image;

        $res = exif_imagetype($path_image);
        switch ($res) {
            case 1:
                $this->_image = imagecreatefromgif($path_image);
                break;
            case 2:
                $this->_image = imagecreatefromjpeg($path_image);
                break;
            case 3:
                $this->_image = imagecreatefrompng($path_image);
                break;
            case 6:
                $this->_image = imagecreatefromwbmp($path_image);
                break;
            default:
                throw new \Exception("Formato de imagen no compatible!");
                break;
        }
        $this->_image_w = getimagesize($path_image)[0];
        $this->_image_h = getimagesize($path_image)[1];
    }

    public function set_max_noise($noise)
    {
        $this->max_noise = $noise;
        return $this;
    }
    public function set_max_grey($grey)
    {
        $this->max_grey = $grey;
        return $this;
    }
    public function set_min_grey($grey)
    {
        $this->min_grey = $grey;
        return $this;
    }

    public function set_letter_width($width)
    {
        $this->letter_width = $width;
        return $this;
    }
    public function set_letter_height($height)
    {
        $this->letter_height = $height;
        return $this;
    }

    public function set_min_width_letter($width)
    {
        $this->min_width_letter = $width;
        return $this;
    }

    public function set_min_height_letter($height)
    {
        $this->min_height_letter = $height;
        return $this;
    }
    /*
		 * return array $data
		 *
		 * */
    public function rgb2hash_grey()
    {
        for ($i = 0; $i < $this->_image_h; $i++) {
            for ($j = 0; $j < $this->_image_w; $j++) {
                $rgb = imagecolorat($this->_image, $j, $i);
                $rgb_array = imagecolorsforindex($this->_image, $rgb);
                $this->_hash_data_grey[$i][$j] = intval(($rgb_array['red'] + $rgb_array['green'] + $rgb_array['blue']) / 3);
            }
        }
        return $this;
    }
    /*
	* return array $data
	*
	* */
    public function rgb2hash()
    {
        if ($this->max_grey === null) {
            throw new \Exception("max_grey no definido!", 1);
        }
        if ($this->min_grey === null) {
            throw new \Exception("min_grey no definido!", 1);
        }

        for ($i = 0; $i < $this->_image_h; $i++) {
            for ($j = 0; $j < $this->_image_w; $j++) {
                $rgb = imagecolorat($this->_image, $j, $i);
                $rgb_array = imagecolorsforindex($this->_image, $rgb);
                $grey = intval(($rgb_array['red'] + $rgb_array['green'] + $rgb_array['blue']) / 3);
                if ($grey >= $this->min_grey && $grey <= $this->max_grey) {
                    $this->_hash_data[$i][$j] = 1;
                } else {
                    $this->_hash_data[$i][$j] = 0;
                }
            }
        }
        return $this;
    }

    public function rotate_hash($hash_data = NULL)
    {
        $x = 0;
        $y = 0;
        $tmp = array();
        $hash = ($hash_data == NULL) ? $this->_hash_data : $hash_data;
        foreach ($hash as $i => $a) {
            foreach ($a as $j => $v) {
                $tmp[$x][$y] = $v;
                $x++;
            }
            $y++;
            $x = 0;
        }
        $this->_hash_tmp = $tmp;
        if ($hash_data !== NULL) {
            return $this->_hash_tmp;
        }
        $this->_hash_data = $tmp;
        return $this;
    }

    private function contador(&$hash, $x, $y)
    {
        $contador = 0;
        // R
        $_x = 1;
        $_y = 0;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //RB
        $_x = 1;
        $_y = 1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //B
        $_x = 0;
        $_y = 1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //LB
        $_x = -1;
        $_y = 1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //L
        $_x = -1;
        $_y = 0;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //LT
        $_x = -1;
        $_y = -1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //T
        $_x = 0;
        $_y = -1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //RT
        $_x = 1;
        $_y = -1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        return $contador;
    }
    private function clean_noise($x, $y)
    {
        $this->_hash_data[$y][$x] = 0;
        // R
        $_x = 1;
        $_y = 0;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //RB
        $_x = 1;
        $_y = 1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //B
        $_x = 0;
        $_y = 1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //LB
        $_x = -1;
        $_y = 1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //L
        $_x = -1;
        $_y = 0;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //LT
        $_x = -1;
        $_y = -1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //T
        $_x = 0;
        $_y = -1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //RT
        $_x = 1;
        $_y = -1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
    }
    public function remove_noise()
    {
        if (empty($this->_hash_data)) {
            $this->rgb2hash();
        }

        $hash = $this->_hash_data;
        foreach ($hash as $y => &$fila) {
            foreach ($fila as $x => &$value) {
                if ($value === 1) {
                    $value = 0;
                    $contador = $this->contador($hash, $x, $y);
                    if ($contador < $this->max_noise) {
                        $this->clean_noise($x, $y);
                    }
                }
            }
        }
        return $this;
    }
    public function remove_zero($hash_data = NULL)
    {
        $hash = ($hash_data == NULL) ? $this->_hash_data : $hash_data;

        $bt = true;
        $bb = true;
        $bl = true;
        $br = true;
        // top
        reset($hash);
        while ($bt == true) {
            $i = key($hash);
            if (implode("", current($hash)) == 0) {
                unset($hash[$i]);
                reset($hash);
            } else {
                $bt = false;
            }
        }
        // Botttom
        end($hash);
        while ($bb == true) {
            $i = key($hash);
            if (implode("", current($hash)) == 0) {
                unset($hash[$i]);
                end($hash);
            } else {
                $bb = false;
            }
        }
        $this->_hash_tmp = $hash;
        if ($hash_data !== NULL) {
            return $this->_hash_tmp;
        }
        $this->_hash_data = $hash;
        return $this;
    }

    public function remove_borde($hash_data = NULL)
    {
        if ($hash_data !== NULL) {
            $hash_data = $this->remove_zero($hash_data);
            $hash_data = $this->rotate_hash($hash_data);
            $hash_data = $this->remove_zero($hash_data);
            $hash_data = $this->rotate_hash($hash_data);
            return $hash_data;
        }
        $this->remove_zero()->rotate_hash()->remove_zero()->rotate_hash();
        return $this;
    }

    private function create_hash($n, $m)
    {
        $hash = array();
        for ($i = 0; $i < $n; $i++) {
            $hash[$i] = array_fill(0, $m, 0);
        }
        return $hash;
    }

    public function extract_letters()
    {
        $this->_hash_tmp = $this->_hash_data;
        $letters = array();
        $i = count($this->_hash_tmp) / 2; //eje y
        foreach ($this->_hash_tmp[$i] as $j => $v) {
            if ($this->_hash_tmp[$i][$j] == 1) {
                $letters[] = $this->extract_letter($i, $j);
            }
        }
        $this->_hash_letters = array();
        foreach ($letters as $i => $v) {
            $l = $this->remove_borde($v);
            $h = count($l);
            $l = $this->rotate_hash($l);
            $l = $this->remove_borde($l);
            $w = count($l);
            $l = $this->rotate_hash($l);
            if (($w * $h) > ($this->min_height_letter * $this->min_width_letter)) {
                $this->_new_image_w += $w + 2;
                $this->_new_image_h = ($h >= $this->_new_image_h - 2) ? ($h + 2) : $this->_new_image_h;
                $this->_hash_letters[] = $l;
            }
        }
        $this->_count_letter = count($this->_hash_letters);
        return $this;
    }

    private function extract_letter($i, $j)
    {
        $n = count($this->_hash_tmp);
        $m = count($this->_hash_tmp[0]);
        $letter = $this->create_hash($n, $m);

        $this->recorre($letter, $i, $j);

        return $letter;
    }

    private function recorre(&$letter, $i, $j)
    {
        // punto
        if ($this->_hash_tmp[$i][$j] != 0) {
            $letter[$i][$j] = 1;
            $this->_hash_tmp[$i][$j] = 0;
        }
        // top
        if (isset($this->_hash_tmp[$i - 1][$j]) && $this->_hash_tmp[$i - 1][$j] != 0) {
            $this->recorre($letter, $i - 1, $j);
        }
        // bottom
        if (isset($this->_hash_tmp[$i + 1][$j]) && $this->_hash_tmp[$i + 1][$j] != 0) {
            $this->recorre($letter, $i + 1, $j);
        }
        // left
        if (isset($this->_hash_tmp[$i][$j - 1]) && $this->_hash_tmp[$i][$j - 1] != 0) {
            $this->recorre($letter, $i, $j - 1);
        }
        // right
        if (isset($this->_hash_tmp[$i][$j + 1]) && $this->_hash_tmp[$i][$j + 1] != 0) {
            $this->recorre($letter, $i, $j + 1);
        }
    }

    public function letter_center()
    {
        return $this;
    }

    public function save_image($file = null)
    {
        if (empty($this->_hash_data)) {
            throw new \Exception("texto no encontrado!", 1);
        }
        if ($this->_image_w == 0) {
            throw new \Exception("alto de imagen no definida!", 1);
        }
        if ($this->_image_h == 0) {
            throw new \Exception("ancho de imagen no definida!", 1);
        }
        $this->_save_image = @imagecreate($this->_image_w, $this->_image_h) or die('No se puede Iniciar el nuevo flujo a la imagen GD');
        $bg = imagecolorallocate($this->_save_image, 255, 255, 255);

        foreach ($this->_hash_data as $y => $v) {
            foreach ($v as $x => $color) {
                if ($color == 1) {
                    imagesetpixel($this->_save_image, $x, $y, 0xFFFFFF);
                }
            }
        }

        if ($file != null) {
            imagejpeg($this->_save_image, $file, 100);
        }
        return $this;
    }

    public function create_new_image($file = null)
    {
        if (empty($this->_hash_letters)) {
            throw new \Exception("texto no encontrado!", 1);
        }
        if ($this->_new_image_w == 0) {
            throw new \Exception("alto de imagen no definida!", 1);
        }
        if ($this->_new_image_h == 0) {
            throw new \Exception("ancho de imagen no definida!", 1);
        }
        $this->_new_image = @imagecreate($this->_new_image_w, $this->_new_image_h) or die('No se puede Iniciar el nuevo flujo a la imagen GD');
        $bg = imagecolorallocate($this->_new_image, 255, 255, 255);

        $p = 0;
        $pos_x = 1;
        foreach ($this->_hash_letters as $letter) {
            $i = 0;
            foreach ($letter as $y => $v) {
                $j = 0;
                foreach ($v as $x => $color) {
                    if ($color == 1) {
                        imagesetpixel($this->_new_image, $pos_x + $j, $i + 1, 0xFFFFFF); // pixel negro
                    }
                    $j += 1;
                }
                $i += 1;
            }
            $pos_x += count(reset($letter)) + 2;
            $p += 1;
        }
        if ($file != null) {
            imagejpeg($this->_new_image, $file, 100);
        }
        return $this;
    }

    public function dump_letters()
    {
        foreach ($this->_hash_letters as $i => $letter) {
            foreach ($letter as $i => $v) {
                echo implode("", $v) . "\n";
            }
            echo "\n";
        }
        return $this;
    }

    public function dump_hash()
    {
        foreach ($this->_hash_data as $v) {
            echo implode("", $v) . "\n";
        }
        return $this;
    }

    public function get_hash_letters()
    {
        return $this->_hash_letters;
    }

    public function get_count_letters()
    {
        return $this->_count_letter;
    }

    public function get_hash_data()
    {
        return $this->_hash_data;
    }

    /* TESSERACT-OCR */
    public function tesseract()
    {
        $this->path_image_tmp = $this->path_image;

        $this->_tesseractOCR = new TesseractOCR();

        if ($this->path_image_tmp !== NULL) {
            $this->_tesseractOCR->image($this->path_image_tmp);
            return $this->_tesseractOCR;
        }

        $this->_tesseractOCR->image($this->path_image);
        return $this->_tesseractOCR;
    }
}
