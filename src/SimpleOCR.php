<?php

namespace jossmp\ocr;

class SimpleOCR
{
    public $letter_width = 30;
    public $letter_height = 30;

    public $max_grey = 255;
    public $min_grey = 0;

    public $min_width_letter = 10;
    public $min_height_letter = 15;

    private $_image;
    private $_image_w;
    private $_image_h;

    private $_hash_data = [];
    private $_hash_data_grey = [];
    private $_hash_letters = [];

    public $path_image = NULL;
    public $path_image_tmp = NULL;

    public $max_noise = 30;

    public function set_image_string($image, $format = null)
    {
        $this->_image = imagecreatefromstring($image);

        if ($this->_image !== false) {
            $this->_image_w  = imagesx($this->_image);
            $this->_image_h  = imagesy($this->_image);

            $this->_hash_data = [];
            $this->_hash_data_grey = [];
            $this->_hash_letters = [];
        } else {
            throw new ImageNotFoundException("Cadena de imagen no compatible!");
        }
        return $this;
    }

    public function set_image_path($path_image)
    {
        FriendlyErrors::checkImagePath($path_image);

        $this->path_image = $path_image;

        $res = exif_imagetype($path_image);
        switch ($res) {
            case 1:
                $this->_image = imagecreatefromgif($path_image);
                break;
            case 2:
                $this->_image = imagecreatefromjpeg($path_image);
                break;
            case 3:
                $this->_image = imagecreatefrompng($path_image);
                break;
            case 6:
                $this->_image = imagecreatefromwbmp($path_image);
                break;
            default:
                throw new \Exception("Formato de imagen no compatible!");
                break;
        }

        $this->_image_w  = imagesx($this->_image);
        $this->_image_h  = imagesy($this->_image);

        $this->_hash_data = [];
        $this->_hash_data_grey = [];
        $this->_hash_letters = [];
        return $this;
    }

    public function set_max_grey($grey)
    {
        $this->max_grey = $grey;
        return $this;
    }
    public function set_min_grey($grey)
    {
        $this->min_grey = $grey;
        return $this;
    }

    public function set_letter_width($width)
    {
        $this->letter_width = $width;
        return $this;
    }
    public function set_letter_height($height)
    {
        $this->letter_height = $height;
        return $this;
    }

    public function rgb2hash_grey()
    {
        for ($i = 0; $i < $this->_image_h; $i++) {
            for ($j = 0; $j < $this->_image_w; $j++) {
                $rgb = imagecolorat($this->_image, $j, $i);
                $rgb_array = imagecolorsforindex($this->_image, $rgb);
                $this->_hash_data_grey[$i][$j] = intval(($rgb_array['red'] + $rgb_array['green'] + $rgb_array['blue']) / 3);
            }
        }
        return $this;
    }
    /*
	* return array $data
	*
	* */
    public function rgb2hash()
    {
        if ($this->max_grey === null) {
            throw new \Exception("max_grey no definido!", 1);
        }
        if ($this->min_grey === null) {
            throw new \Exception("min_grey no definido!", 1);
        }

        for ($i = 0; $i < $this->_image_h; $i++) {
            for ($j = 0; $j < $this->_image_w; $j++) {
                $rgb = imagecolorat($this->_image, $j, $i);
                $rgb_array = imagecolorsforindex($this->_image, $rgb);
                $grey = intval(($rgb_array['red'] + $rgb_array['green'] + $rgb_array['blue']) / 3);
                if ($grey >= $this->min_grey && $grey <= $this->max_grey) {
                    $this->_hash_data[$i][$j] = 1;
                } else {
                    $this->_hash_data[$i][$j] = 0;
                }
            }
        }
        return $this;
    }

    private function contador(&$hash, $x, $y)
    {
        $contador = 0;
        // R
        $_x = 1;
        $_y = 0;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //RB
        $_x = 1;
        $_y = 1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //B
        $_x = 0;
        $_y = 1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //LB
        $_x = -1;
        $_y = 1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //L
        $_x = -1;
        $_y = 0;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //LT
        $_x = -1;
        $_y = -1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //T
        $_x = 0;
        $_y = -1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        //RT
        $_x = 1;
        $_y = -1;
        if (isset($hash[$y + $_y][$x + $_x]) && $hash[$y + $_y][$x + $_x] == 1) {
            $contador++;
            $hash[$y + $_y][$x + $_x] = 0;
            $contador = $contador + $this->contador($hash, $x + $_x, $y + $_y);
        }
        return $contador;
    }
    private function clean_noise($x, $y)
    {
        $this->_hash_data[$y][$x] = 0;
        // R
        $_x = 1;
        $_y = 0;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //RB
        $_x = 1;
        $_y = 1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //B
        $_x = 0;
        $_y = 1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //LB
        $_x = -1;
        $_y = 1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //L
        $_x = -1;
        $_y = 0;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //LT
        $_x = -1;
        $_y = -1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //T
        $_x = 0;
        $_y = -1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
        //RT
        $_x = 1;
        $_y = -1;
        if (isset($this->_hash_data[$y + $_y][$x + $_x]) && $this->_hash_data[$y + $_y][$x + $_x] == 1) {
            $this->_hash_data[$y + $_y][$x + $_x] = 0;
            $this->clean_noise($x + $_x, $y + $_y);
        }
    }
    public function remove_noise()
    {
        if (empty($this->_hash_data)) {
            $this->rgb2hash();
        }

        $hash = $this->_hash_data;
        foreach ($hash as $y => &$fila) {
            foreach ($fila as $x => &$value) {
                if ($value === 1) {
                    $value = 0;
                    $contador = $this->contador($hash, $x, $y);
                    if ($contador < $this->max_noise) {
                        $this->clean_noise($x, $y);
                    }
                }
            }
        }
        return $this;
    }

    public function dump_hash()
    {
        if (empty($this->_hash_data)) {
            $this->rgb2hash();
        }
        foreach ($this->_hash_data as $v) {
            echo implode("", $v) . "\n";
        }
        return $this;
    }

    public function get_hash_letters()
    {
        return $this->_hash_letters; // [][]
    }

    public function get_count_letters()
    {
        return count($this->_hash_letters); // int
    }

    public function get_hash_data()
    {
        return $this->_hash_data; // [][]
    }
    public function __call($name, $arguments)
    {
        if (method_exists($this, $name)) {
            return call_user_func_array(array($this, $name), $arguments);
        }
        throw new \Exception("Method $name not found!");
    }

    public function add_letter($x, $y, $w, $h)
    {
        if (empty($this->_hash_data)) {
            $this->rgb2hash();
        }

        $letter = [];

        for ($i = 0; $i < $h; $i++) {
            for ($j = 0; $j <  $w; $j++) {
                $letter[$i][$j] = $this->_hash_data[$i + $y][$j + $x];
                // echo $letter[$i][$j];
            }
            // echo "\n";
        }
        // echo "\n";
        $this->_hash_letters[] = $letter;
        return $this;
    }
    public function save_letters($path_save)
    {
        if (empty($this->_hash_letters)) {
            throw new \Exception("No hay letras para guardar!");
        }
        $name_file = basename($this->path_image);
        $name = str_split($name_file);
        foreach ($this->_hash_letters as $i => $letter) {
            $str_leter = "";
            foreach ($letter as $v) {
                $str_leter .= implode("", $v) . "\n";
            }
            file_put_contents($path_save . "/" . $i + 1 . "/" . $name[$i] . ".txt", $str_leter);
        }
        return $this;
    }

    private function compare_letter($letter, $_letter)
    {
        $diff      = 0;
        $count   = 0;
        $success = 0;
        $fail      = 0;

        foreach ($_letter as $i => $l) {
            foreach ($l as $j => $v) {
                if ($letter[$i][$j] == 1 && $v == 1) {
                    $count++;
                    $success++;
                } else if ($letter[$i][$j] == 1 && $v == 0) {
                    $count++;
                    $diff++;
                    $fail++;
                } else if ($letter[$i][$j] == 0 && $v == 1) {
                    $count++;
                    $fail++;
                }
            }
        }
        // $pdiff    = round(($diff * 100) / ($success + $fail));
        // $pfail    = round(($fail * 100) / ($success + $fail));
        // $psuccess = round(($success * 100) / ($success + $fail));
        $pdiff    = round(($diff * 100) / $count);
        $pfail    = round(($fail * 100) / ($count));
        $psuccess = round(($success * 100) / ($count));
        // echo "|F:" . $pfail . "%| |S:" . $psuccess . "%| |D:" . $pdiff . "%|\n";
        // return ($pdiff < 10) ? (($psuccess > $pfail) ? $psuccess : 0) : (($diff > $psuccess) ? 0 : $psuccess);
        return ($psuccess > $pfail) ? $psuccess : $psuccess * 0.9;
    }

    function solver($dictionary)
    {
        if (empty($this->_hash_letters)) {
            throw new \Exception("No hay letras para resolver!");
        }

        $str_leter = "";

        //foreach ($this->_hash_letters as $i => $letter) {
        $str_leter .= $this->compare_letters($this->_hash_letters[0], $dictionary[0]);
        $str_leter .= $this->compare_letters($this->_hash_letters[1], $dictionary[1]);
        $str_leter .= $this->compare_letters($this->_hash_letters[2], $dictionary[2]);
        $str_leter .= $this->compare_letters($this->_hash_letters[3], $dictionary[3]);
        $str_leter .= $this->compare_letters($this->_hash_letters[4], $dictionary[4]);
        $str_leter .= $this->compare_letters($this->_hash_letters[5], $dictionary[5]);
        //$str_leter .= $this->compare_letters($this->_hash_letters[6], $dictionary[6]);
        //}

        return $str_leter;
    }

    function compare_letters($letter, $dictionary)
    {
        $mayor = 0;
        $str_letter = null;
        foreach ($dictionary as $i => $_letter) {
            $similarity[$i] = $this->compare_letter($letter, $_letter);
            if ($similarity[$i] > $mayor) {
                $mayor = $similarity[$i];
                $str_letter = $i;
            }
        }
        //echo $str_letter . " -> " . intval($mayor) . "\n";
        return trim($str_letter);
    }

    // Image Tesseract
    private function save_image($file = null)
    {
        if (empty($this->_hash_data)) {
            throw new \Exception("texto no encontrado!", 1);
        }
        if ($this->_image_w == 0) {
            throw new \Exception("alto de imagen no definida!", 1);
        }
        if ($this->_image_h == 0) {
            throw new \Exception("ancho de imagen no definida!", 1);
        }
        $this->_image = @imagecreate($this->_image_w, $this->_image_h) or die('No se puede Iniciar el nuevo flujo a la imagen GD');
        $bg = imagecolorallocate($this->_image, 255, 255, 255);

        foreach ($this->_hash_data as $y => $v) {
            foreach ($v as $x => $color) {
                if ($color == 1) {
                    imagesetpixel($this->_image, $x, $y, 0xFFFFFF);
                }
            }
        }

        if ($file != null) {
            imagejpeg($this->_image, $file, 100);
        }
        return $this;
    }
    public function tesseract()
    {
        $this->path_image_tmp = sys_get_temp_dir() . '/' . uniqid(rand(100, 999)) . '.jpg';
        $this->save_image($this->path_image_tmp);

        $_tesseractOCR = new TesseractOCR();

        if ($this->path_image_tmp !== NULL) {
            $_tesseractOCR->image($this->path_image_tmp);
            return $_tesseractOCR;
        }
        throw new \Exception("Imagen no se ha grabado!", 1);
        // $_tesseractOCR->image($this->path_image);
        // return $_tesseractOCR;
    }
}
